package sofiyev.nurlan.helper;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.imgscalr.Scalr;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import sofiyev.nurlan.db.dao.DataBaseImpl;
import sofiyev.nurlan.db.model.Photo;
import sofiyev.nurlan.utils.Utils;
import net.sf.json.JSONObject;

public class HelperUtils {
	
	private static final Logger logger = Logger.getLogger(HelperUtils.class);
	
	/**
	 * 
	 * @param str
	 * @return string
	 * encoding with md5 HelperUtils.getMD5Hex(str)
	 */
	public static String getMD5Hex(String str){
		return DigestUtils.md5Hex(str);	
	}

	/**
	 * 
	 * @param str
	 * @return string
	 * encoding with sha1 HelperUtils.getSha1Hex(str)
	 */
	public static String getSha1Hex(String str){
		return DigestUtils.sha1Hex(str);	
	}
	
	public static void startSessionWithTransaction(Session session, Transaction transaction) {
		session = DataBaseImpl.getSessionFactory().openSession();
		transaction  = session.getTransaction();
		transaction.begin();
		
	}

	public static void writeTransactionalDAOExceptionToDb(Exception e, Transaction transaction) {
		 if (transaction != null) {
             transaction.rollback();
		 }
		 logger.error(e);	
	}

	public static void closeSession(Session session) {
		if(session!=null){
			session.close();
		}		
	}

	public static void writeDAOExceptionToDb(Exception e) {
		 e.printStackTrace(); //write to db
	}

	public static Integer getInteger(HttpServletRequest request, String param) {
		String paramStr = request.getParameter(param);
		try{
			return Integer.parseInt(paramStr);
		} catch(Exception e){
			return null;
		}
	}
	
	public static Double getDouble(HttpServletRequest request, String param) {
		String paramStr = request.getParameter(param);
		try{
			return Double.parseDouble(paramStr);
		} catch(Exception e){
			return null;
		}
	}
	public static String getString(HttpServletRequest request, String param){
		String paramStr = request.getParameter(param);
		try {
			return paramStr.trim();
		}catch (Exception e){
			return null;
		}
	}

	public static byte[] getPhotoFromPath(Integer photoId, Boolean thumb) {
			thumb = false;
		 String sRootPath = System.getProperty("catalina.base");
		 File resizedFile = new File(sRootPath+"/uploads/photos/"+(thumb?"resized/"+Utils.thumbnailSize+"/":"original/")+photoId+".png");
		 try {
			InputStream fis = new FileInputStream(resizedFile);
			return IOUtils.toByteArray(fis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		
		return null;
	}

	//save photo to resized and original folders of root folder
	public static void savePhotoToPath(Blob image, Photo userphoto) throws IOException, SQLException {
		logger.info(" savePhotoToPath : "+userphoto.getId());
		try{
			 byte[] bytes = IOUtils.toByteArray(image.getBinaryStream());
			 InputStream bufferedImageByteInputStream = new ByteArrayInputStream(bytes); 
			 BufferedImage img = ImageIO.read(bufferedImageByteInputStream); 
			 BufferedImage scaledImg = Scalr.resize(img, Utils.thumbnailSize);				 
			 ByteArrayOutputStream os = new ByteArrayOutputStream();
			 ImageIO.write(scaledImg, "PNG", os);		
			 String sRootPath = System.getProperty("catalina.base");
			 logger.info("sRootPath:"+sRootPath);
			 logger.info("filepath:"+sRootPath+"/uploads/photos/original/"+userphoto.getId()+".png");
			 File originalFile = new File(sRootPath+"/uploads/photos/original/"+userphoto.getId()+".png");
			 originalFile.getParentFile().mkdirs(); 
			 originalFile.createNewFile();
			 ImageIO.write(img, "PNG", originalFile);	
			 File resizedFile = new File(sRootPath+"/uploads/photos/resized/"+Utils.thumbnailSize+"/"+userphoto.getId()+".png");
			 resizedFile.getParentFile().mkdirs(); 
			 resizedFile.createNewFile();
			 ImageIO.write(scaledImg, "PNG", resizedFile);
		} catch(Exception e){
			logger.error(e);
		}
		
		
	}

	public static Boolean getBoolean(HttpServletRequest request, String param) {
		String paramStr = request.getParameter(param);
		try{
			return Boolean.parseBoolean(paramStr);
		} catch(Exception e){
			return null;
		}
	}

	public static String getStringFromJSON(JSONObject json, String string) {
		try{
			if(json.has(string)){
				return json.getString(string);
			}
		} catch(Exception e){
		}
		return null;
	}

	public static Integer getIntFromJSON(JSONObject json, String string) {
		try{
			if(json.has(string) ){
				return json.getInt(string);
			}
		} catch(Exception e){
		}
		
		return null;
	}	

	public static Boolean getBooleanFromJSON(JSONObject json, String string) {
		try{
			if(json.has(string) ){
				return json.getBoolean(string);
			}
		} catch(Exception e){
		}
		
		return null;
	}

	public static void writeExceptionToDb(Exception e) {
		logger.error(e);
	}	
	
	public static Date incrementDate(Date date, int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();      
        cal.setTimeInMillis(date.getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

	public static Date getDateFromJSON(JSONObject json, String key, String formatStr) {
		try{
			if(json.has(key) ){
				String dateStr =  json.getString(key);
				if(Utils.isNullOrEmpty(dateStr)){
					return null;
				}
				DateFormat format = new SimpleDateFormat(formatStr);
				return format.parse(dateStr);
			}
		} catch(Exception e){
		}
		
		return null;
	}

	public static Double getDoubleFromJSON(JSONObject json, String string) {
		try{
			if(json.has(string) ){
				return json.getDouble(string);
			}
		} catch(Exception e){
		}
		
		return null;
	}
	
	 public static List<GrantedAuthority> getAuthorities(Integer role) {
	        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
	        if (role.intValue() == 1) {
	            authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

	        } else if (role.intValue() == 2) {
	            authList.add(new SimpleGrantedAuthority("ROLE_USER"));
	        }
	        System.out.println(authList);
	        return authList;
	    }
	
}
