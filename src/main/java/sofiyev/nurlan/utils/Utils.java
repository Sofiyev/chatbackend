package sofiyev.nurlan.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import sofiyev.nurlan.helper.HelperUtils;

import net.sf.json.JSONObject;


public class Utils {


	private static final Logger logger = Logger.getLogger(Utils.class);
	
	public enum NotificationReceivingType {
		NONE(0), EMAIL(1), NOTIFICATION(2), ALL(3);
		public int value;
		NotificationReceivingType(int value) {
			this.value = value;
		}
	}
	
	
	public enum StoryTypeRelationType {
		NONE(0), STORYTELLER(1), TRAVELTIP(2);
		public int value;
		StoryTypeRelationType(int value) {
			this.value = value;
		}
		private static StoryTypeRelationType[] allValues = values();
		public static StoryTypeRelationType fromStoryTypeRelationType(int n) {return allValues[n];}
	}
	

	

	
	public enum TokenType {
		NONE(0), PASSWORDRESET(1), VERIFICATION(2);
		public int value;
		TokenType(int value) {
			this.value = value;
		}
	}



	public static Integer thumbnailSize = 100;
	static String dataFile = "data.properties";
	static String messagesFile = "messages_en_US.properties";
	public static HashMap<String,String> messages = new HashMap<String,String>();
	static {
		try {
			 Properties properties = new Properties();
			 InputStream	input = Utils.class.getClassLoader().getResourceAsStream(messagesFile);
			 properties.load(input);
				for (String key : properties.stringPropertyNames()) {
				    String value = properties.getProperty(key);
				    messages.put(key, value);
				}		
			} catch(Exception ex){			
				logger.error(ex);
			}
	}
	
	
	public static void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws  IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
		while (src.read(buffer) != -1) {
			buffer.flip();
			dest.write(buffer);
			buffer.compact();
		}
		buffer.flip();
		while (buffer.hasRemaining()) {
			dest.write(buffer);
		}
	}
	public static String getUsername(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth!=null && auth.getPrincipal() instanceof String){
			return (String)auth.getPrincipal();//unauthorized
		}
		String username = ((org.springframework.security.core.userdetails.User)auth.getPrincipal()).getUsername();
		return username;
	}
	
	
	public static boolean isNullOrEmpty(String param) { 
		return param==null || param.isEmpty() || param.equals("undefined");
	}
	
	public static boolean isEmptyList(List list) {
	    return (list == null || list.isEmpty());
	}
	
	/**
	 * This is for saving all requested data as JSON object.
	 * We will log this data .If exception happened during database actions we will understand which 
	 * data user entered
	 * @param request
	 * @return JSONObject
	 */
	public static JSONObject requestToInputString(HttpServletRequest request){
		JSONObject inputJSON = new JSONObject();	
		Map<String, String[]> map = request.getParameterMap();
		for(Object key : map.keySet()){
			String keyStr = (String)key;
			Object value = map.get(keyStr);     
			inputJSON.put(keyStr, value);		 
		}
		return inputJSON;
	}

	/*
	 * Used to get in which method exception happened
	 */
	public static String getMethodName()
	{
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		String methodName="";
		for(int i=0;i<stacktrace.length;i++){
			StackTraceElement e = stacktrace[i];
			if(e!=null && e.getMethodName()!=null ){
				methodName+=e.getMethodName()+" ";
			}
		}
		return methodName;
	}
	

	
	public static Date getDateFromJson(JSONObject jsonObj, String key, String format) throws ParseException {
		try{
			if(jsonObj != null && !Utils.isNullOrEmpty(key) && jsonObj.has(key) && jsonObj.get(key) instanceof String){
				String dateString = jsonObj.getString(key);
				DateFormat formatter = new SimpleDateFormat(format);
				Date dtObj = (Date)formatter.parse(dateString);
				return dtObj;
			}
		} catch(Exception e){
			HelperUtils.writeExceptionToDb(e);
		}
		
		return null;		
	}
	
	public static void setHeaders(HttpServletResponse response){
		System.out.println("setting headers");
		response.setHeader("Access-Control-Allow-Origin", "*");
		// Request methods you wish to allow
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		// Request headers you wish to allow
		response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		// Set to true if you need the website to include cookies in  requests
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setCharacterEncoding("UTF-8");
	}
	
	
	
}
