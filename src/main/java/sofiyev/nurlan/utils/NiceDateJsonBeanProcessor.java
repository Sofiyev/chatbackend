package sofiyev.nurlan.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public  class NiceDateJsonBeanProcessor implements JsonValueProcessor {
    @Override
    public Object processArrayValue(Object value, JsonConfig jsonConfig) {
        return process(value, jsonConfig);
    }
    @Override
    public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
        return process(value, jsonConfig);
    }
    private Object process(Object value, JsonConfig jsonConfig) {
        if (value instanceof java.util.Date) {
            java.util.Date d = (java.util.Date) value;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return df.format(d);
        }
        return null;
    }
}