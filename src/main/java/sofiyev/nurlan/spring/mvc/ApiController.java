package sofiyev.nurlan.spring.mvc;

import io.jsonwebtoken.Claims;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sofiyev.nurlan.db.dao.DataBaseImpl;
import sofiyev.nurlan.db.dao.MessageDAO;
import sofiyev.nurlan.db.dao.PhotoDAO;
import sofiyev.nurlan.db.dao.TokenDAO;
import sofiyev.nurlan.db.dao.UserDAO;
import sofiyev.nurlan.db.model.Message;
import sofiyev.nurlan.db.model.MessageThread;
import sofiyev.nurlan.db.model.Photo;
import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;
import sofiyev.nurlan.spring.security.DbUserDetailsService;
import sofiyev.nurlan.utils.NiceDateJsonBeanProcessor;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class ApiController {
	
	@Autowired
	private UserDAO userDAO;

	@Autowired
	private PhotoDAO photoDAO;

	@Autowired
	private MessageSource messages;

	@Autowired
	private TokenDAO tokenDAO;


	@Autowired
	private DbUserDetailsService userDetailsService;


	@Autowired
	private MessageDAO messageDAO;

	public static JsonConfig jsonConfig = new JsonConfig();

	@PostConstruct 
	public void initMethod() throws ClassNotFoundException {
		jsonConfig.setExcludes(new String[] { "userInfo", "friendshipsFrom", "friendshipsTo", "userPhotos", "roles",
				"image", "messageThreadParticipants", "messages", "posts", "groupParticipants","password" });
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Class.forName("java.util.Date"), new NiceDateJsonBeanProcessor());
	}

	/**
	 * updates user and userinfo data from settings account details page
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateAccountDetails", method = RequestMethod.POST)
	public void updateAccountDetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {

			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);
			user.setFirstname(request.getParameter("firstname"));
			user.setLastname(request.getParameter("lastname"));
			user.setEmail(request.getParameter("email"));
			// hibernate validation
			Validator validator = DataBaseImpl.validatorFactory.getValidator();
			Set<ConstraintViolation<User>> validationErrors = validator.validate(user);
			JSONObject errorFormObj = new JSONObject();
			if (!validationErrors.isEmpty()) {
				for (ConstraintViolation<User> error : validationErrors) {
					errorFormObj.put(error.getPropertyPath(), error.getPropertyPath() + " " + error.getMessage());
				}
				throw new Exception(errorFormObj.toString());
			}

			userDAO.saveOrUpdate(user);

			sendJSON.put("success", true);
			sendJSON.put("msg", "Account information updated successfully");
		} catch (Exception ex) {
			sendJSON.put("success", false);
			sendJSON.put("msg", ex.getMessage());
		}

		out.print(sendJSON);
		out.close();
	}

	@RequestMapping(value = "/getAccountDetails", method = RequestMethod.POST)
	public void getAccountDetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {

			String username = (String)((Claims)request.getAttribute("claims")).get("sub");

			User user = userDAO.findByUsername(username);
			if (user != null) {
				sendJSON.put("user", JSONObject.fromObject(user, jsonConfig));
				sendJSON.put("success", true);
			}else{
				sendJSON.put("success", false);
				sendJSON.put("msg", "no user found !");
			}

		} catch (Exception ex) {
			sendJSON.put("success", false);
			sendJSON.put("msg", ex.getMessage());
		}

		out.print(sendJSON);
		out.close();
	}

	@RequestMapping(value = "/uploadPhoto", method = RequestMethod.POST)
	public void uploadPhoto(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		FileItemIterator items = upload.getItemIterator(request);
		while (items.hasNext()) {
			FileItemStream fis = items.next();
			if (!fis.isFormField()) {
				InputStream input = fis.openStream();
				try {
					String username = (String)((Claims)request.getAttribute("claims")).get("sub");
					User user = userDAO.findByUsername(username);				
					Photo userPhotoNew = new Photo();
					userPhotoNew.setRelatedId(user.getId());

					Date now = new Date();
					userPhotoNew.setCreateDate(now);
					photoDAO.saveOrUpdatePicture(input, userPhotoNew);
					sendJSON.put("success", true);
					sendJSON.put("msg", messages.getMessage("photoUpdateSuccess", null, request.getLocale()));
				} catch (Exception e) {
					e.printStackTrace();
					HelperUtils.writeExceptionToDb(e);
					sendJSON.put("success", false);
					sendJSON.put("msgDetail", e.getMessage());
					sendJSON.put("msg", messages.getMessage("photoUpdateError", null, request.getLocale()));
				}
			}
		}
		out.print(sendJSON);
		out.close();
	}

	@RequestMapping(value = "/getProfilePicture2", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getProfilePicture2(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		try {
			Photo userPhoto = null;
			Boolean thumb= HelperUtils.getBoolean(request, "thumb");
			if(thumb==null)thumb=false;

				String username = request.getParameter("username");
				Integer userId = HelperUtils.getInteger(request, "userId");
				if(userId!=null){
					User user =userDAO.get(userId);
					username = user.getUsername();
				}
				if (username == null) {
					username = (String)((Claims)request.getAttribute("claims")).get("sub");
				}
				User user = userDAO.findByUsername(username);
				if (user == null) {
					return null;
				}
				userPhoto = photoDAO.getLastPhoto(user.getId());

			if(userPhoto==null){
				return null;// no profile photo
			}
			Integer photoId = userPhoto.getId();

			byte[] imgData = HelperUtils.getPhotoFromPath(photoId, thumb);

			if (imgData == null) {
				HelperUtils.savePhotoToPath(userPhoto.getImage(), userPhoto); // updates
				imgData = HelperUtils.getPhotoFromPath(photoId, thumb);

			}
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.IMAGE_JPEG);
			return new ResponseEntity<byte[]>(imgData, headers, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "/getProfilePicture", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getProfilePicture(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		try {
			Photo userPhoto = null;
			Boolean isTask = HelperUtils.getBoolean(request, "isTask");
			Boolean thumb= HelperUtils.getBoolean(request, "thumb");
			if(thumb==null)thumb=false;
			if (isTask != null && isTask) {
				Integer taskId = HelperUtils.getInteger(request, "taskId");
				userPhoto = photoDAO.getLastPhoto(taskId);
			} else {
				String username = request.getParameter("username");
				Integer userId = HelperUtils.getInteger(request, "userId");
				if(userId!=null){
					User user =userDAO.get(userId);
					username = user.getUsername();
				}
				if (username == null) {
					username = (String)((Claims)request.getAttribute("claims")).get("sub");
				}
				User user = userDAO.findByUsername(username);
				if (user == null) {
					return null;
				}
				userPhoto = photoDAO.getLastPhoto(user.getId());
			}
			if(userPhoto==null){
				return null;// no profile photo
			}
			Integer photoId = userPhoto.getId();
			
			byte[] imgData = HelperUtils.getPhotoFromPath(photoId, thumb); // if exist:
			if (imgData == null) {
				HelperUtils.savePhotoToPath(userPhoto.getImage(), userPhoto);
				imgData = HelperUtils.getPhotoFromPath(photoId, thumb);

			}
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.IMAGE_JPEG);
			return new ResponseEntity<byte[]>(imgData, headers, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/createMessage", method = RequestMethod.POST)
	public void createMessage(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			Date sendTime = new Date();
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);

			String messageContent = request.getParameter("messageContent");
			User to = userDAO.findByUsername( request.getParameter("to") );

			List<MessageThread> messageThreadList = null;

			if( user.getId() > to.getId() ){
				messageThreadList = messageDAO.getMessageThreadByParams(to,user);
			}else{
				messageThreadList = messageDAO.getMessageThreadByParams(user,to);
			}

			MessageThread messageThread = messageThreadList.size()>0?messageThreadList.get(0):null;
			if(messageThread==null){
				messageThread = new MessageThread();
				//messageThread.setId(1);

				messageThread.setCreateDate(new Date());
				if( user.getId() > to.getId() ) {
					messageThread.setFrom(to);
					messageThread.setTo(user);
				}else{
					messageThread.setFrom(user);
					messageThread.setTo(to);
				}

				messageDAO.saveMessageThread(messageThread);

			}

			Message message = new Message();
			message.setText(messageContent);
			if(messageThread.getFrom().getId().equals(user.getId())){
				message.setUser(messageThread.getFrom() );
			} else{
				message.setUser(messageThread.getTo() );
			}
			message.setCreateDate(sendTime);
			message.setMessageThread(messageThread);
			messageDAO.saveMessage(message);
			sendJSON.put("success", true);
			sendJSON.put("messageId", message.getId());
			sendJSON.put("messageThreadId", messageThread.getId());
		} catch (Exception e) {
			e.printStackTrace();
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}


	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
	public void sendMessage(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			Date now = new Date();
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);
			String messageContent = request.getParameter("messageContent");
			Integer messageThreadId = HelperUtils.getInteger(request, "messageThreadId");
			MessageThread messageThread = messageDAO.getMessageThread(messageThreadId);
			Message message = new Message();
			message.setText(messageContent);
			message.setMessageThread(messageThread);
			if(messageThread.getTo().getId().equals(user.getId())){
				message.setUser(messageThread.getTo());
			} else{
				message.setUser(messageThread.getFrom());
			}
			message.setCreateDate(now);
			messageDAO.saveMessage(message);
			sendJSON.put("messageId", message.getId());
			User notificationUser = messageThread.getTo();
			if(notificationUser==user){
				notificationUser = messageThread.getFrom();
			}

			sendJSON.put("success", true);
		} catch (Exception e) {
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}

	@RequestMapping(value = "/getUsers", method = RequestMethod.POST)
	public void getUsers(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			//String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			//User user = userDAO.findByUsername(username);
			//Boolean countUnread = HelperUtils.getBoolean(request, "countUnread");
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);

			List<User> users = messageDAO.getUsers();
			JSONArray userArray = new JSONArray();
			for (int i = 0; i < users.size(); i++) {
				JSONObject userObj = new JSONObject();
				User to = users.get(i);
				List<MessageThread> messageThreadList = null;
				if( user.getId() > to.getId() ){
					messageThreadList = messageDAO.getMessageThreadByParams(to,user);
				}else{
					messageThreadList = messageDAO.getMessageThreadByParams(user,to);
				}
				MessageThread messageThread = messageThreadList.size()>0?messageThreadList.get(0):null;
				if( messageThread == null ){
					userObj.put("messageThreadId",0);
				}else{
					userObj.put("messageThreadId",messageThread.getId());
				}

				userObj.put("user", JSONObject.fromObject(to, jsonConfig));
				userArray.add(userObj);
			}
			sendJSON.put("data", userArray);
			sendJSON.put("success", true);
		} catch (Exception e) {
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}


	@RequestMapping(value = "/getLastMessageOfThreads", method = RequestMethod.POST)
	public void getLastMessageOfThreads(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);
			Integer start = HelperUtils.getInteger(request, "start");
			Integer limit = HelperUtils.getInteger(request, "limit");
			Boolean unread = HelperUtils.getBoolean(request, "unread");
			Boolean countUnread = HelperUtils.getBoolean(request, "countUnread");
			List<Message> messageList = messageDAO.getLastMessageOfThreads(user, unread, start, limit);
			JSONArray messageArray = new JSONArray();
			for (int i = 0; i < messageList.size(); i++) {
				JSONObject messageObj = new JSONObject();
				messageObj.put("message", JSONObject.fromObject(messageList.get(i), jsonConfig));
				messageArray.add(messageObj);
			}
			sendJSON.put("count", messageDAO.getCountMessageThread(user, countUnread));
			sendJSON.put("data", messageArray);
			sendJSON.put("user", username);
			sendJSON.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}


	@RequestMapping(value = "/getMessagesOfThread", method = RequestMethod.POST)

	public void getMessagesOfThread(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			User user = userDAO.findByUsername(username);
			Integer start = HelperUtils.getInteger(request, "start");
			Integer limit = HelperUtils.getInteger(request, "limit");
			Integer messageThreadId = HelperUtils.getInteger(request, "messageThreadId");
			MessageThread messageThread = messageDAO.getMessageThread(messageThreadId);
			List<Message> messageList = messageDAO.getMessagesOfThread(messageThread, start, limit, username );
			messageDAO.setMessageRead(messageThread, user);			
			sendJSON.put("data", JSONArray.fromObject(messageList, jsonConfig));
			sendJSON.put("count", messageDAO.getMessagesCountOfThread(messageThread));
			sendJSON.put("success", true);
			sendJSON.put("user", (String)((Claims)request.getAttribute("claims")).get("sub"));
		} catch (Exception e) {
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}

	@RequestMapping(value = "/getNewMessages", method = RequestMethod.POST)
	public void getNewMessages(HttpServletRequest request, HttpServletResponse response)
			throws FileUploadException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			Integer lastId = HelperUtils.getInteger(request, "lastId");
			Integer messageThreadId = HelperUtils.getInteger(request, "messageThreadId");
			String username = (String)((Claims)request.getAttribute("claims")).get("sub");
			MessageThread messageThread = messageDAO.getMessageThread(messageThreadId);
			List<Message> messageList = messageDAO.getNewMessages(messageThread, lastId, username);
			sendJSON.put("data", JSONArray.fromObject(messageList, jsonConfig));
			sendJSON.put("count", messageDAO.getMessagesCountOfThread(messageThread));
			sendJSON.put("success", true);
			sendJSON.put("user", (String)((Claims)request.getAttribute("claims")).get("sub"));
		} catch (Exception e) {
			HelperUtils.writeExceptionToDb(e);
			sendJSON.put("success", false);
			sendJSON.put("msgDetail", e.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}


}
