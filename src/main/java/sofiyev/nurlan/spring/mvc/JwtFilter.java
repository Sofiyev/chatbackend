package sofiyev.nurlan.spring.mvc;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.GenericFilterBean;

import sofiyev.nurlan.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;


public class JwtFilter extends GenericFilterBean {

    @Override
    public void doFilter(final ServletRequest req,
                         final ServletResponse res,
                         final FilterChain chain) throws IOException, ServletException {

    	final HttpServletRequest request = (HttpServletRequest) req;
    	HttpServletResponse response = (HttpServletResponse) res; 
    	Utils.setHeaders(response);
    	response.setCharacterEncoding("UTF-8");
    	final String authHeader = request.getParameter("token");

    	if (authHeader == null) {
    		throw new ServletException("Missing or invalid Authorization header.");
    	}
    	try {
    		final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(authHeader).getBody();
    		request.setAttribute("claims", claims);          
    	}
    	catch (final SignatureException e) {
    		throw new ServletException("Invalid token.");
    	}



    	chain.doFilter(req, res);
    }

}
