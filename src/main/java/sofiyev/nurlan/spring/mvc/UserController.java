package sofiyev.nurlan.spring.mvc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sofiyev.nurlan.db.dao.DataBaseImpl;
import sofiyev.nurlan.db.dao.MessageDAO;
import sofiyev.nurlan.db.dao.TokenDAO;
import sofiyev.nurlan.db.dao.UserDAO;
import sofiyev.nurlan.db.model.Token;
import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;
import sofiyev.nurlan.spring.security.DbUserDetailsService;
import sofiyev.nurlan.utils.Utils;
import sofiyev.nurlan.utils.Utils.TokenType;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.sf.json.JSONObject;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private MessageSource messages;

	@Autowired
	private TokenDAO tokenDAO;

	@Autowired
	private DbUserDetailsService userDetailsService;



	@Autowired
	private MessageDAO messageDAO;

	private final Map<String, List<String>> userDb = new HashMap<>();

	private static final Logger logger = Logger.getLogger(UserController.class);
	
    public UserController() {
        userDb.put("tom", Arrays.asList("user"));
        userDb.put("sally", Arrays.asList("user", "admin"));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login( HttpServletRequest request, HttpServletResponse response) throws IOException{
    	Utils.setHeaders(response);
    	PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String msg = null;
		User userdb = null;
		try{			
			//UserDetails userdb = userDetailsService.loadUserByUsername(username);
			userdb = userDAO.findByUsernameOrEmail(username);
			if(userdb.getPassword().equals(HelperUtils.getSha1Hex(password))){
				
				sendJSON.put("success", true);
			} else{
				System.out.println("failure");
				sendJSON.put("success", false);
				msg = "wrongPassword";
			}

		} catch(Exception ex){
			ex.printStackTrace();
			sendJSON.put("success", false);
		}
		if(!Utils.isNullOrEmpty(msg)){
			sendJSON.put("msg", msg);
		} else{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE,1);
			sendJSON.put("msg", "Login successful");
			String token = Jwts.builder().setSubject(userdb.getUsername())
		            .claim("roles", Arrays.asList("ROLE_USER")).setIssuedAt(new Date())
		            .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
			sendJSON.put("token", token);
			sendJSON.put("userId", userdb.getId());
		}
		out.print(sendJSON);
		out.close();
    }
    
	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public void signUp(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Utils.setHeaders(response);
		PrintWriter out = response.getWriter();
		JSONObject sendJSON = new JSONObject();
		try {
			JSONObject jsonUser = JSONObject.fromObject(request.getParameter("user"));
			User user = (User) JSONObject.toBean(jsonUser, User.class);
			// hibernate validation
			Validator validator = DataBaseImpl.validatorFactory.getValidator();
			Set<ConstraintViolation<User>> validationErrors = validator.validate(user);
			JSONObject errorFormObj = new JSONObject();
			if (!validationErrors.isEmpty()) {
				for (ConstraintViolation<User> error : validationErrors) {
					errorFormObj.put(error.getPropertyPath(),  error.getMessage());
				}
				throw new Exception(errorFormObj.toString());
			}

			// username & email validation
			if (userDAO.findUserByEmail(user.getEmail()) != null) {
				errorFormObj.put("email", messages.getMessage("regErrorEmail", null, request.getLocale()));
				throw new Exception(errorFormObj.toString()); // TODO locale
			}
			if (userDAO.findByUsername(user.getUsername()) != null) {
				errorFormObj.put("username", messages.getMessage("regErrorUsername", null, request.getLocale()));
				throw new Exception(errorFormObj.toString()); // TODO locale
			}

			user.setPassword(HelperUtils.getSha1Hex(user.getPassword()));
			user.setCreateDate(new Date());
			userDAO.saveOrUpdate(user);
			sendJSON.put("success", true);
			sendJSON.put("email", user.getEmail());
			sendJSON.put("username", user.getUsername());
			String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
			final String tokenString = UUID.randomUUID().toString();
			Token token = new Token(tokenString, user, TokenType.VERIFICATION);
			tokenDAO.save(token);
		} catch (Exception ex) {
			sendJSON.put("success", false);
			sendJSON.put("formErrors", ex.getMessage());
		}
		out.print(sendJSON);
		out.close();
	}
	

    @SuppressWarnings("unused")
    private static class UserLogin {
        public String name;
        public String password;
    }

    @SuppressWarnings("unused")
    private static class LoginResponse {
        public String token;

        public LoginResponse(final String token) {
            this.token = token;
        }
    }
}
