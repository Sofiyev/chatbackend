/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sofiyev.nurlan.spring.security;

import javax.annotation.Resource;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import sofiyev.nurlan.db.dao.UserDAO;
import sofiyev.nurlan.helper.HelperUtils;


/**
 *
 * @author anjana.m
 */
@Component
public class DbUserDetailsService implements UserDetailsService {

    @Resource
    private UserDAO userDAO;


    private org.springframework.security.core.userdetails.User userdetails;

    public UserDetails loadUserByUsername(String username)
    		throws UsernameNotFoundException {
    	boolean accountNonExpired = true;
    	boolean credentialsNonExpired = true;
    	boolean accountNonLocked = true;
    	sofiyev.nurlan.db.model.User user = getUserDetail(username);

    	userdetails = new User(user.getUsername(), 
    			user.getPassword(),
    			true,
    			accountNonExpired,
    			credentialsNonExpired,
    			accountNonLocked,
    			HelperUtils.getAuthorities(2)); //TODO
    	return userdetails;
    }

   

    public sofiyev.nurlan.db.model.User getUserDetail(String username) {
    	sofiyev.nurlan.db.model.User user = userDAO.findByUsernameOrEmail(username);
        return user;
    }


   
}