package sofiyev.nurlan.db.dao;
import java.util.List;

import sofiyev.nurlan.db.model.User;


public interface UserDAO {
	
	public void saveOrUpdate(User user);
		
	public User get(Integer userId);
	
	public List<User> list();

	User findByUsername(String username);

	List<User> listUsers();

	User findUserByEmail(String userEmail);


	public User findByUsernameOrEmail(String username);

	
}
