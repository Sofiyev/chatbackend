package sofiyev.nurlan.db.dao;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import sofiyev.nurlan.db.model.Photo;

public interface PhotoDAO {

	public Photo getLastPhoto(Integer relatedId);

	void saveOrUpdatePicture(InputStream input, Photo userphoto) throws SQLException, IOException;
		
	List<Photo> getUserPhotos(Integer relatedId, Integer start, Integer limit);

	Photo get(Integer photoId);

	void saveOrUpdatePhoto(Photo photo);

}
