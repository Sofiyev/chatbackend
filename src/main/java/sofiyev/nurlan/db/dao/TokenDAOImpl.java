package sofiyev.nurlan.db.dao;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sofiyev.nurlan.db.model.Token;
import sofiyev.nurlan.helper.HelperUtils;
import sofiyev.nurlan.utils.Utils.TokenType;

public class TokenDAOImpl implements TokenDAO {

	@Override
	public Token getToken(String token, TokenType tokenType) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();		
			String str = " from Token where token=:token and type=:tokenType";
			Query query = session.createQuery(str);
			query.setParameter("token", token);
			query.setParameter("tokenType", tokenType);
			@SuppressWarnings("unchecked")
			List<Token> lst = (List<Token>)query.list();		
			if(lst.size()>0){
				return lst.get(0);
			}
		} catch(Exception e){
			HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}
		return null;
	}

	@Override
	public void save(Token token) {
		Session session = null; Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			session.saveOrUpdate(token);
			transaction.commit();	
		} catch(Exception e){
			HelperUtils.writeTransactionalDAOExceptionToDb(e,transaction);	    
			
		} finally{
			HelperUtils.closeSession(session);			
		}
	}

}
