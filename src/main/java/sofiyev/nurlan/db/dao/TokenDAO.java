package sofiyev.nurlan.db.dao;
import sofiyev.nurlan.db.model.Token;
import sofiyev.nurlan.utils.Utils.TokenType;


public interface TokenDAO {

	Token getToken(String token, TokenType tokenType);

	void save(Token token);
	
}
