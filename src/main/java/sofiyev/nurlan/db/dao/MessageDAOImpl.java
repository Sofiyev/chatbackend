package sofiyev.nurlan.db.dao;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sofiyev.nurlan.db.model.Message;
import sofiyev.nurlan.db.model.MessageThread;
import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;
import sofiyev.nurlan.utils.Utils;

public class MessageDAOImpl implements MessageDAO {

	@Override
	public void saveMessageThread(MessageThread messageThread) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			Boolean isNew = false;
			if(messageThread.getId()==null){
				//new message thread => send email to to to notify
				isNew = true;
               // messageThread.setId(12);
			}
			System.out.println(messageThread.getId());
			session.saveOrUpdate(messageThread);
			//if(isNew && messageThread.getType().value==MessageThreadType.TRAVELPLAN.value){
			//	UserNotification userNotification = new UserNotification(null, messageThread.getTo(), messageThread.getFrom(), messageThread.getCreateDate(), NotificationSettingType.MESSAGEFIRSTTOTRAVELER, NotificationReceivingType.EMAIL, true, false, messageThread.getId());
			//	session.saveOrUpdate(userNotification);
			//} else if(isNew && messageThread.getType().value==MessageThreadType.TASK.value){
			//	UserNotification userNotification = new UserNotification(null, messageThread.getTo(), messageThread.getFrom(), messageThread.getCreateDate(), NotificationSettingType.MESSAGEFIRSTTOCUSTOMER, NotificationReceivingType.EMAIL, true, false, messageThread.getId());
			//	session.saveOrUpdate(userNotification);
			//}
			transaction.commit();

		} catch(Exception e){
			HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
			e.printStackTrace();
		} finally{
			HelperUtils.closeSession(session);
		}		
	}

	@Override
	public void saveMessage(Message message) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			session.save(message);
			transaction.commit();
		} catch(Exception e){
		    e.printStackTrace();
			  HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
		} finally{
			HelperUtils.closeSession(session);
		}			
	}

	@Override
	public MessageThread getMessageThread(Integer messageThreadId) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery(" from MessageThread where id=:messageThreadId");
			query.setParameter("messageThreadId", messageThreadId);
			@SuppressWarnings("unchecked")
			List<MessageThread> messageThreadList  =  query.list();
			if(!Utils.isEmptyList(messageThreadList) && messageThreadList.size()>0){
				return messageThreadList.get(0);
			}
		} catch(Exception e){
			  HelperUtils.writeDAOExceptionToDb(e);
            e.printStackTrace();
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}

	@Override
	public List<Message> getMessagesOfThread(MessageThread messageThread, Integer start, Integer limit, String username) {
		Session session = null;
		Logger log = java.util.logging.Logger.getLogger("MessageDAOImpl ");
		try{


			//log.info("Burdayam" + username);
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery(" from Message where  messageThread=:messageThread  order by id desc");
			query.setParameter("messageThread", messageThread);

			if(start!=null && limit!=null){
				query.setFirstResult(start);
				query.setMaxResults(limit);
			} 
			@SuppressWarnings("unchecked")
			List<Message> messageList  =  query.list();
			return messageList;
		} catch(Exception e){
			  HelperUtils.writeDAOExceptionToDb(e);
			  log.info(e.toString());
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}

	@Override
	public Long getMessagesCountOfThread(MessageThread messageThread) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery("select count(id) from Message where messageThread=:messageThread");
			query.setParameter("messageThread", messageThread);
			Long countMessage  =  (Long) query.uniqueResult();
			return countMessage;
		} catch(Exception e){
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}

	@Override
	public List<Message> getNewMessages(MessageThread messageThread, Integer lastId, String username) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery(" from Message where messageThread=:messageThread  and id>:lastId order by id desc");
			query.setParameter("messageThread", messageThread);
			query.setParameter("lastId", lastId);
			@SuppressWarnings("unchecked")
			List<Message> messageList  =  query.list();
			return messageList;
		} catch(Exception e){
			e.printStackTrace();
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}
	
	@Override
	public Message getMessage( Integer messageId) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery(" from Message where id=:messageId");
			query.setParameter("messageId", messageId);
			@SuppressWarnings("unchecked")
			List<Message> messageList  =  query.list();
			if(messageList!=null && messageList.size()>0){
				return messageList.get(0);
			}
		} catch(Exception e){
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}

	@Override
	public void setMessageRead(MessageThread mt, User user) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			Query query = session.createQuery(" update Message message set responseDate=:responseDate where message.messageThread=:messageThread and user!=:sendingUser and responseDate is null");
			query.setParameter("responseDate", new Date());
			query.setParameter("messageThread", mt);
			query.setParameter("sendingUser", user);
			query.executeUpdate();
			transaction.commit();		
		} catch(Exception e){
			  HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
		} finally{
			HelperUtils.closeSession(session);
		}		
	}

	@Override
	public List<User> getUsers(){
		Session session = null;
		try {
			session = DataBaseImpl.getSessionFactory().openSession();
			String queryStr = "from User ";
			Query query = session.createQuery(queryStr);
			List<User> list = query.list();
			return list;
		}catch (Exception e){
			e.printStackTrace();
			HelperUtils.writeDAOExceptionToDb(e);
		}finally {
			HelperUtils.closeSession(session);
		}
		return null;
	}

	@Override
	public List<Message> getLastMessageOfThreads(User user, Boolean unread, Integer start, Integer limit) {
		Session session = null;
		try{
			String unreadCondition = unread?" and m.responseDate is null ":"";
			session = DataBaseImpl.getSessionFactory().openSession();
			String queryStr = "select m from MessageThread mt left join mt.messages m "
					+ " where (mt.to=:user or mt.from=:user) "+unreadCondition +" and m.id=(select max(m1.id) from Message m1 where m1.messageThread=mt) group by mt  order by m.createDate desc";
			Query query = session.createQuery(queryStr);
			query.setParameter("user", user);
			if(start!=null && limit!=null){
				query.setFirstResult(start);
				query.setMaxResults(limit);
			} 
			@SuppressWarnings("unchecked")
			List<Message> messageList = query.list();
			return messageList;
		} catch(Exception e){
		    e.printStackTrace();
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}
	
	@Override
	public Long getCountMessageThread(User user, Boolean unread) {
		Session session = null;
		try{
			String queryStr = null;
			if(unread){
//				queryStr = "select count(mt) from MessageThread mt left join mt.messages m  "
//						+ " where (mt.to=:user or mt.from=:user) and m.user!=:user and m.responseDate is null group by mt ";
				queryStr = "select count(mt) from MessageThread mt where (mt.to=:user or mt.from=:user) and (select count(m.id) from Message m where m.user!=:user and m.responseDate is null and m.messageThread=mt)>0";
			} else{
				queryStr = "select count(mt) from MessageThread mt  where (mt.to=:user or mt.from=:user)";
			}
			session = DataBaseImpl.getSessionFactory().openSession();			
			Query query = session.createQuery(queryStr);
			query.setParameter("user", user);
			Long count = (Long) query.uniqueResult();
			if(count==null){
				return 0l;
			}
			return count;
		} catch(Exception e){
			e.printStackTrace();
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}


    @Override
    public List<MessageThread> getMessageThreadByParams(User af, User az) {
        Session session = null;
        try{
            session = DataBaseImpl.getSessionFactory().openSession();
            String queryStr = " from MessageThread mt where  ";
            queryStr += " mt.from=:af";
            queryStr += " and mt.to=:az";
            Query query = session.createQuery(queryStr);
            query.setParameter("af", af);
            query.setParameter("az", az);

            @SuppressWarnings("unchecked")
            List<MessageThread> messageThreadList = query.list();
            return messageThreadList;
        } catch(Exception e){
            HelperUtils.writeDAOExceptionToDb(e);
        } finally{
            HelperUtils.closeSession(session);
        }
        return null;
    }

	@Override
	public List<MessageThread> getMessageThreadByParams(User user, Integer relatedId, Boolean isFrom, Integer start, Integer limit) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			String queryStr = " from MessageThread mt where  ";
			if(isFrom==null){
				queryStr += " (from=:user or to=:user)";
			} else if(isFrom==true){
				queryStr += " from=:user";
			} else {
				queryStr += " to=:user";
			}
			if(relatedId!=null){
				queryStr += " and relatedId=:relatedId";
			}

			Query query = session.createQuery(queryStr);
			query.setParameter("user", user);
			if(relatedId!=null){
				query.setParameter("relatedId", relatedId);
			}
			
			@SuppressWarnings("unchecked")
			List<MessageThread> messageThreadList = query.list();
			return messageThreadList;
		} catch(Exception e){
			  HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}	
		return null;
	}

}
