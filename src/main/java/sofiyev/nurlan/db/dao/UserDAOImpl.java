package sofiyev.nurlan.db.dao;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;

public class UserDAOImpl implements UserDAO {

	@Override
	public void saveOrUpdate(User user) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			session.saveOrUpdate(user);
			transaction.commit();	
		} catch(Exception e){
			 HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
			
		} finally{
			HelperUtils.closeSession(session);
		}
	}


	@Override
	public User get(Integer userId) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			User user = (User)session.get(User.class, userId);
			return user;
		} catch(Exception e){
			HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> list() {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery("from User where enabled=true");
			List<User> users = (List<User>)query.list();
			return users;
		} catch(Exception e){
			//add to db
		} finally{
			if(session!=null){
				session.close();
			}
		}
		return null;
	}

	
	
	@Override
	public User findByUsername(String username) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();		
			String str = " from User where username=:username";
			Query query = session.createQuery(str);
			query.setParameter("username", username);
			@SuppressWarnings("unchecked")
			List<User> lst = (List<User>)query.list();
		
			if(lst.size()>0){
				return lst.get(0);
			}
		} catch(Exception e){
			
			//add to db
		} finally{
			if(session!=null){
				session.close();
			}
		}
		return null;
	}

	@Override
	public List<User> listUsers() {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();		
			String str = " from User";
			Query query = session.createQuery(str);
			@SuppressWarnings("unchecked")
			List<User> lst = (List<User>)query.list();
			return lst;
		} catch(Exception e){
			//add to db
		} finally{
			if(session!=null){
				session.close();
			}
		}
		return null;
	}

	@Override
	public User findUserByEmail(final String email) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();		
			String str = " from User where email=:email";
			Query query = session.createQuery(str);
			query.setParameter("email", email);
			@SuppressWarnings("unchecked")
			List<User> lst = (List<User>)query.list();
		
			if(lst.size()>0){
				return lst.get(0);
			}
			
		} catch(Exception e){
			//add to db
		} finally{
			if(session!=null){
				session.close();
			}
		}
		return null;
    }




	




	@Override
	public User findByUsernameOrEmail(String username) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();		
			String str = " from User where username=:username or email=:username";
			Query query = session.createQuery(str);
			query.setParameter("username", username);
			@SuppressWarnings("unchecked")
			List<User> lst = (List<User>)query.list();
		
			if(lst.size()>0){
				return lst.get(0);
			}
		} catch(Exception e){
			e.printStackTrace();
			HelperUtils.writeExceptionToDb(e);
		} finally{
			if(session!=null){
				session.close();
			}
		}
		return null;
	}


	

	


	
}
