package sofiyev.nurlan.db.dao;
import java.io.InputStream;
import java.sql.Blob;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sofiyev.nurlan.db.model.Photo;
import sofiyev.nurlan.helper.HelperUtils;


public class PhotoDAOImpl implements PhotoDAO {

	private static final Logger logger = Logger.getLogger(PhotoDAOImpl.class);

	@Override
	public void saveOrUpdatePicture(InputStream input, Photo userphoto)  {
		
		 Session session = null;
		 Transaction transaction = null;
		 try{
			 session = DataBaseImpl.getSessionFactory().openSession();
			 transaction = session.getTransaction();
			 transaction.begin();
			 if(input!=null){
				 byte[] bytes = IOUtils.toByteArray(input);
				 Blob blob = Hibernate.getLobCreator(session).createBlob(bytes);
				 userphoto.setImage(blob);
				 session.saveOrUpdate(userphoto);		    
				 transaction.commit();
				 HelperUtils.savePhotoToPath(blob, userphoto);
			 }
		
		 } catch(Exception e){
			 logger.error(e);
			 HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
		 } finally{
			 if(session!=null){
				 session.close();
			 }
		 }
				
	}

	@Override
	public Photo getLastPhoto(Integer relatedId) {
		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			Query query = session.createQuery(" from Photo where relatedId=:relatedId "
					+ "order by createDate desc ");
			query.setParameter("relatedId", relatedId);
			query.setMaxResults(1);//top 1
			@SuppressWarnings("unchecked")
			List<Photo> userPhotoList = query.list();
			if(userPhotoList.size()>0){
				return userPhotoList.get(0);
			}

		} catch(Exception e){
			e.printStackTrace();
			HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}
		return null;
	}



	@Override
	public List<Photo> getUserPhotos(Integer relatedId, Integer start, Integer limit)  {

		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			String queryStr = " from Photo where relatedId=:relatedId  ";
			queryStr+=" order by id desc";
			Query query = session.createQuery(queryStr);

			query.setParameter("relatedId", relatedId);
			if(start!=null && limit!=null){
				query.setFirstResult(start);
				query.setMaxResults(limit);
				
			}
			@SuppressWarnings("unchecked")
			List<Photo> userPhotoList = query.list();
			return userPhotoList;
		} catch(Exception e){
			HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}
		return null;
	}
	
	@Override
	public Photo get(Integer photoId)  {

		Session session = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			String queryStr = " from Photo where id=:id";		
			Query query = session.createQuery(queryStr);		
			query.setParameter("id", photoId);			
			@SuppressWarnings("unchecked")
			List<Photo> userPhotoList = query.list();
			if(userPhotoList.size()>0){
				return userPhotoList.get(0);
			}
			
		} catch(Exception e){
			HelperUtils.writeDAOExceptionToDb(e);
		} finally{
			HelperUtils.closeSession(session);
		}
		return null;
	}


	@Override
	public void saveOrUpdatePhoto(Photo photo) {
		Session session = null;
		Transaction transaction = null;
		try{
			session = DataBaseImpl.getSessionFactory().openSession();
			transaction  = session.getTransaction();
			transaction.begin();
			session.saveOrUpdate(photo);
			transaction.commit();	
		} catch(Exception e){
			 HelperUtils.writeTransactionalDAOExceptionToDb(e, transaction);
			
		} finally{
			HelperUtils.closeSession(session);
		}
	}



}
