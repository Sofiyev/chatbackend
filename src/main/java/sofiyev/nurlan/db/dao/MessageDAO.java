package sofiyev.nurlan.db.dao;
import java.util.List;
import sofiyev.nurlan.db.model.Message;
import sofiyev.nurlan.db.model.MessageThread;
import sofiyev.nurlan.db.model.User;

public interface MessageDAO {

	void saveMessageThread(MessageThread messageThread);

	void saveMessage(Message message);

	MessageThread getMessageThread(Integer messageThreadId);

	List<Message> getMessagesOfThread(MessageThread messageThread, Integer start, Integer limit, String Username);

	Long getMessagesCountOfThread(MessageThread messageThread);

	List<Message> getNewMessages(MessageThread messageThread, Integer lastId, String username);

	Message getMessage(Integer messageId);

	void setMessageRead(MessageThread mt, User user);

	List<Message> getLastMessageOfThreads(User user, Boolean unread, Integer start, Integer limit);

	List<User> getUsers();

	Object getCountMessageThread(User user, Boolean unread);

	List<MessageThread> getMessageThreadByParams(User user, Integer relatedId, Boolean isFrom, Integer start, Integer limit);
    List<MessageThread> getMessageThreadByParams(User from, User to);


}
