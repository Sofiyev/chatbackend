package sofiyev.nurlan.db.dao;
import java.util.Set;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;
 

public class DataBaseImpl {

	public static SessionFactory sessionFactory;
	public static ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
	public static void connect(){
		Configuration configuration = null;
		try {

			String cfgFile="/hibernate.cfg.xml"; 
			configuration = new Configuration().configure(cfgFile);		 
			Reflections reflections = new Reflections("sofiyev.nurlan.db.model");
			Set<Class<?>> classes = reflections.getTypesAnnotatedWith(javax.persistence.Entity.class);
			for(Class<?> clazz : classes)
			{
				configuration.addAnnotatedClass(clazz);
			}		       
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
			sessionFactory = configuration.buildSessionFactory(builder.build());

		} catch (Exception he) {
			configuration=null;
			sessionFactory=null;
			System.out.println("session db problem"+he.getMessage());

		}
	}

	public static SessionFactory getSessionFactory() { 
		if(sessionFactory==null) connect();
		return sessionFactory;  
	}

}
