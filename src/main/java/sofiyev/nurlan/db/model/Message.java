package sofiyev.nurlan.db.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name="message") 
public class Message {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id")
	Integer id;
	
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
    @JoinColumn(name = "message_thread_id", referencedColumnName="id")
	MessageThread messageThread;
	
	@ManyToOne(optional=false,cascade=CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName="id")
	User user;

	@Column(name="text")
	String text;


	@Column(name="create_date")
	Date createDate;
	
	@Column(name="response_date")
	Date responseDate;

	public Message() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MessageThread getMessageThread() {
		return messageThread;
	}

	public void setMessageThread(MessageThread messageThread) {
		this.messageThread = messageThread;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}


}
