package sofiyev.nurlan.db.model;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import sofiyev.nurlan.helper.HelperUtils;
import sofiyev.nurlan.utils.Utils.TokenType;

@Entity
@Table(name="token")
public class Token {

    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @Column(name="token", length=50)
    private String token;

	@ManyToOne(optional=false,cascade=CascadeType.ALL)
    @JoinColumn(name = "user", referencedColumnName="id")
	User user;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="type")
	TokenType type;
  
	@Column(name="expiration_Date")
    private Date expirationDate;

    @Column(name="active", length=1)
	Boolean active;
    
    public Token() {
        super();
    }

    public Token(final String token) {
        super();
        this.token = token;
        this.expirationDate = HelperUtils.incrementDate(new Date(),EXPIRATION);
    }

    public Token(final String token, final User user, TokenType type) {
        super();
        this.active=true;
        this.token = token;
        this.user = user;
        this.type = type;
        this.expirationDate = HelperUtils.incrementDate(new Date(),EXPIRATION);
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TokenType getType() {
		return type;
	}

	public void setType(TokenType type) {
		this.type = type;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
