package sofiyev.nurlan.db.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;



@Entity
@Table( name="users") 	
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@NotNull(message="Please enter the username")
	@NotEmpty(message="Please enter the username")
	@Size(min=3, max=50, message="Username length should be between 3 and 50")
	@Column(name="username", unique=true, length=50)
	private String username;
	
	@NotNull(message="Please enter the First name")
	@NotEmpty(message="Please enter the First name")
	@Size(min=3, max=50, message="Firstname length should be between 3 and 50")
	@Column(name="firstname", length=50)
	private String firstname;	
	
	@NotNull(message="Please enter the Last name")
	@NotEmpty(message="Please enter the Last name")
	@Size(min=3, max=50, message="Lastname length should be between 3 and 50")
	@Column(name="lastname", length=50)
	private String lastname;	

	@Email(message="Please provide a valid email address")
	@Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
	@NotNull(message="Please provide a valid email address")
	@NotEmpty(message="Please enter the email")
	@Size(min=5, max=50, message="Email length should be between 5 and 50")
	@Column(name="email", length=50)
	private String email;
	
	@NotNull(message="Please enter password")
	@NotEmpty(message="Please enter password")
	@Column(name="password", length=128)
	private String password;
	
	@Column(name="create_date")
	private Date createDate;
	
	@Column(name="last_login_date")
	private Date lastLoginDate;
	
	@Column(name="role")
	private Integer role=1;
  
    public User() {
        super();
    }
 

	public Integer getId() {
		return id;
	}

 
	public void setId(Integer id) {
		this.id = id;
	}
 

	public String getUsername() {
		return username;
	}

 
	public void setUsername(String username) {
		this.username = username;
	}
 
	
	public String getFirstname() {
		return firstname;
	}

 
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

 
	public String getLastname() {
		return lastname;
	}

 
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

 
	public void setEmail(String email) {
		this.email = email;
	}
 

	public String getPassword() {
		return password;
	}

 
	public void setPassword(String password) {
		this.password = password;
	}
 

	public Date getCreateDate() {
		return createDate;
	}

  
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
 

	public Date getLastLoginDate() {
		return lastLoginDate;
	}
 

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

 
	public Integer getRole() {
		return role;
	}

 
	public void setRole(Integer role) {
		this.role = role;
	}

}
