package integration;

import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reflections.Reflections;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sofiyev.nurlan.db.dao.UserDAO;
import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("config-test.xml")

public class UserIntegrationTest {

	@Resource
	UserDAO userDAO;
	
	public static SessionFactory sessionFactory;
	private Session session = null;
	private Transaction transaction = null;
	
	@Before
    public void before() {
     // setup the session factory
		String cfgFile="hibernate.cfg.xml"; 
		Configuration configuration = new Configuration().configure(cfgFile);
		Reflections reflections = new Reflections("sofiyev.nurlan.db.model");
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(javax.persistence.Entity.class);
        for(Class<?> clazz : classes)
        {
            configuration.addAnnotatedClass(clazz);
        }		
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		sessionFactory = configuration.buildSessionFactory(builder.build());
		session = sessionFactory.openSession();
		transaction = session.getTransaction();
		
    }
	
	
	//YOU TEST HERE
	/***
	 * 	 insert test user, crud be committed
	 */
	@Test
	public void insertUserCommitted(){
		transaction.begin();
		User user = new User();
		user.setCreateDate(new Date());
		user.setEmail("username@email.com");
		user.setFirstname("firstname");
		user.setLastname("lastname");
		user.setUsername("username");
		user.setPassword(HelperUtils.getSha1Hex("password"));
		session.save(user);
		transaction.commit();
		
	}
	

	
	
	@After
	public void after(){
		 session.close();
		
	}
	
	
	
}
