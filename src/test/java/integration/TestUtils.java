package integration;

import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

public class TestUtils {

	public static org.hibernate.SessionFactory createSessionFactory(Session session, Transaction transaction){
		String cfgFile="hibernate.cfg.xml"; 
		Configuration configuration = new Configuration().configure(cfgFile);
		Reflections reflections = new Reflections("sofiyev.nurlan.db.model");
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(javax.persistence.Entity.class);
        for(Class<?> clazz : classes)
        {
            configuration.addAnnotatedClass(clazz);
        }		
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());		
		return sessionFactory;
	}

	public static void afterUtils(Session session) {
		 session.close();		
	}
}
