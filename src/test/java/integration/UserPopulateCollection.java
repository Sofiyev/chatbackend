package integration;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sofiyev.nurlan.db.model.User;
import sofiyev.nurlan.helper.HelperUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("config-test.xml")

public class UserPopulateCollection {
	
	SessionFactory sessionFactory;
	Session session;
	Transaction transaction;
	
    
    
    @Before
    public void createSession(){
    	sessionFactory = TestUtils.createSessionFactory(session, transaction);
		session = sessionFactory.openSession();
		transaction = session.getTransaction();
    }
    
  
    @Test
	public void populateCollectionUser(){
		
    	transaction.begin();
    	for(int i=0;i<10;i++){
    		User user = new User();
    		user.setCreateDate(new Date());
    		user.setEmail("username"+i+"@email.com");
    		user.setFirstname("firstname"+i);
    		user.setLastname("lastname"+i);
    		user.setUsername("username"+i);
    		user.setPassword(HelperUtils.getSha1Hex("password"+i));
    		session.save(user);
    	}
		
		transaction.commit();
		
	}
    
    @After
    public void closeSession(){
   	 	session.close();
    }

}
